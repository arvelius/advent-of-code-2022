class topthree:
    def __init__(self):
        self.vals = [0,0,0]

    def add(self, value):
        if value > self.vals[0]:
            self.vals = [value, self.vals[0], self.vals[1]]
        elif value > self.vals[1]:
            self.vals = [self.vals[0], value, self.vals[1]]
        elif value > self.vals[2]:
            self.vals = [self.vals[0], self.vals[1], value]

    def sum(self):
        return sum(self.vals)

    def __str__(self):
        return str(self.vals)

with open("input.txt", "r") as calorylist:
    top = topthree()
    tmpval = 0
    for line in calorylist:
        try:
            tmpval += int(line)
        except(ValueError):
            top.add(tmpval)
            tmpval = 0
        print(top, tmpval, line)
print(top.sum())
