with open("input.txt", "r") as calorylist:
    maxval = 0
    tmpval = 0
    for line in calorylist:
        try:
            tmpval += int(line)
        except:
            if tmpval > maxval:
                maxval = tmpval
            tmpval = 0
        print(maxval, tmpval, line)
print(maxval)
