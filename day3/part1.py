def splitstring(string):
    return (string[:int(len(string)/2)], string[int(len(string)/2):])

def finderror(string):
    first, second = splitstring(string)
    for char in first:
        if second.find(char) >-1:
            return char

def value(char):
    if ord(char) >90:
        return ord(char)-96
    else:
        return ord(char)-65+27

with open("input.txt") as indata:
    priosum = 0
    for line in indata:
        priosum += value(finderror(line))
print(str(priosum))
