def findduplicates(first, second):
    dupset = set()
    for char in first.strip():
        if second.find(char) >-1:
            dupset.add(char)
    return "".join(dupset)
            
def findtriplicate(indata):
    line1 = indata.readline()
    if line1 == "":
        raise(Exception)
    line2 = indata.readline()
    line3 = indata.readline()
    dups = (findduplicates(line1, line2))
    return findduplicates(dups, line3)
    
def value(char):
    if ord(char) >90:
        return ord(char)-96
    else:
        return ord(char)-65+27

with open("input.txt") as indata:
    priosum = 0
    while indata:
        try:
            priosum += value(findtriplicate(indata))
        except:
            break

print(str(priosum))
