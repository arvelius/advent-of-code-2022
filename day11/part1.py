#!/usr/bin/env python

import sys

if "-t" in sys.argv:
    test=True
    filename = "testdata.txt"
else:
    test=False
    filename = "input.txt"

def readfile(indata):
    monkeys = dict()
    for row in indata:
        if "Monkey" in row:
            number = int(row.rstrip(":\n").split()[1])
            monkey = Monkey(indata)
            monkeys[number] = monkey
        print(row.rstrip('\n'))
    return monkeys

class Monkey():
    def __init__(self, infile):
        data = indata.readline().rstrip('\n').split(":")[1]
        self.items = [int(i) for i in data.split(", ")]
        data = indata.readline().rstrip('\n').split(":")[1]
        self.operation = data.split(" = ")[1]
        data = indata.readline().rstrip('\n').split(":")[1]
        self.divisor = int(data.split()[-1])
        data = indata.readline().rstrip('\n').split(":")[1]
        self.trueaction = int(data.split()[-1])
        data = indata.readline().rstrip('\n').split(":")[1]
        self.falseaction = int(data.split()[-1])
        self.inspections = 0

    def turn(self):
        for old in self.items:
            self.inspections += 1
            level = eval(self.operation)
            level = level//3
            if level%self.divisor == 0:
                reciever = self.trueaction
            else:
                reciever = self.falseaction
            monkeys[reciever].items.append(level)
        self.items=list()

with open(filename, "r") as indata:
    monkeys = readfile(indata)

#if test:
#    assert monkeys[0].items == [79, 98]
#    monkeys[0].turn()
#    assert monkeys[1].items == [54, 65, 75, 74]
for i in range(20):
    for j in range(len(monkeys)):
        monkeys[j].turn()

if test:
    assert monkeys[0].items == [10, 12, 14, 26, 34]
    assert monkeys[1].items == [245, 93, 53, 199, 115]
    assert monkeys[2].items == []
    assert monkeys[3].items == []

    assert monkeys[0].inspections == 101
    assert monkeys[1].inspections == 95
    assert monkeys[2].inspections == 7
    assert monkeys[3].inspections == 105

inspections = [monkeys[i].inspections for i in range(len(monkeys))]
inspections.sort()
print(inspections[-2]*inspections[-1])

