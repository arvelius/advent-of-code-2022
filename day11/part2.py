#!/usr/bin/env python

import sys

if "-t" in sys.argv:
    test=True
    filename = "testdata.txt"
else:
    test=False
    filename = "input.txt"

def readfile(indata):
    monkeys = dict()
    letter = iter("ABCDEFGHIJKLMNPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz")
    for row in indata:
        if "Monkey" in row:
            number = int(row.rstrip(":\n").split()[1])
            monkey = Monkey(indata, letter)
            monkeys[number] = monkey
    return monkeys

class Monkey():
    def __init__(self, infile, letter):
        data = indata.readline().rstrip('\n').split(":")[1]
        self.items = [(int(i), next(letter)) for i in data.split(", ")]
        data = indata.readline().rstrip('\n').split(":")[1]
        self.operation = data.split(" = ")[1]
        data = indata.readline().rstrip('\n').split(":")[1]
        self.divisor = int(data.split()[-1])
        data = indata.readline().rstrip('\n').split(":")[1]
        self.trueaction = int(data.split()[-1])
        data = indata.readline().rstrip('\n').split(":")[1]
        self.falseaction = int(data.split()[-1])
        self.inspections = 0
        
    def turn(self, ringsize):
        for (old, letter) in self.items:
            self.inspections += 1
            level = eval(self.operation)
            level = level%ringsize
            if level%self.divisor == 0:
                reciever = self.trueaction
            else:
                reciever = self.falseaction
            monkeys[reciever].items.append((level, letter))
        self.items=list()
                      
with open(filename, "r") as indata:
    monkeys = readfile(indata)
    ringsize = 1
    for m in monkeys.values():
        ringsize *= m.divisor
#if test:
#    assert monkeys[0].items == [79, 98]
#    monkeys[0].turn()
#    assert monkeys[1].items == [54, 65, 75, 74]
for i in range(10000):
    print(f"round {i}")
    #for j in range(len(monkeys)):
    #    print(f"  {j}: {[k[1] for k in monkeys[j].items]}")
    for j in range(len(monkeys)):
        monkeys[j].turn(ringsize)

if test:
    #assert monkeys[0].items == [10, 12, 14, 26, 34]
    #assert monkeys[1].items == [245, 93, 53, 199, 115]
    #assert monkeys[2].items == []
    #assert monkeys[3].items == []

    #assert monkeys[0].inspections == 99
    #assert monkeys[1].inspections == 97
    #assert monkeys[2].inspections == 8
    #assert monkeys[3].inspections == 103
    pass

inspections = [monkeys[i].inspections for i in range(len(monkeys))]
print(inspections)
inspections.sort()
print(inspections[-2]*inspections[-1])
