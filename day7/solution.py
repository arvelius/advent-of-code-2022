#!/usr/bin/env python

import sys

class Directory(dict):
    def __iter__(self):
        self.iterlist = [k for k,v in self.items() if isinstance(v, Directory) and k != ".."]
        self.i = 0
        self.iterator = iter([])
        self.stopiter = False
        return self

    def __next__(self):
        if self.stopiter:
            raise StopIteration
        try:
            return(next(self.iterator))
        except StopIteration:
            try:
                self.iterator = iter(self[self.iterlist[self.i]])
                self.i += 1
            except IndexError:
                self.stopiter = True
                return self

        return(next(self.iterator))

    def __int__(self):
        return self.size()

    def terminal(self, row):
        parts = row.split()
        if parts[1] == "cd":
            return self[parts[2]]
        elif parts[1] == "ls":
            pass
        elif parts[0] == "dir":
            if parts[1] not in self:
                self[parts[1]] = Directory()
                self[parts[1]]['..'] = self
        else:                                   #output from ls
            self[parts[1]] = int(parts[0])
        return self

    def size(self):
        try:
            return self._size
        except AttributeError:
            self._size=0
            for name, val in self.items():
                if name == "..":
                    pass
                else:
                    self._size += int(val)
            return self._size

if "-t" in sys.argv:
    test=True
    filename = "testdata.txt"
else:
    test=False
    filename = "input.txt"

def readfile(indata):
    indata.readline()
    directory = root = Directory()
    for row in indata:
        directory = directory.terminal(row.rstrip('\n'))
    return(root)

def part1(root):
    thesum = 0
    for directory in root:
        if directory.size() < 100000:
            thesum += directory.size()
    print(thesum)

def part2(root):
    free = 70000000 - root.size()
    need = 30000000 - free
    best = root.size()
    for directory in root:
        if directory.size() > need and directory.size() < best:
            best = directory.size()
    print(best)

with open(filename, "r") as indata:
    root = readfile(indata)
    part1(root)
    part2(root)
