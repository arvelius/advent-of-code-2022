#!/usr/bin/env python

import numpy as np
from string import ascii_lowercase
import sys

if "-t" in sys.argv:
    test=True
    filename = "testdata.txt"
else:
    test=False
    filename = "input.txt"

def sumcoords(c1, c2):
    return c1[0]+c2[0], c1[1]+c2[1]

class Node:
    def __init__(self, value, coords, parent):
        self.value = value
        self.coords = coords
        self.parent = parent

    def __str__(self):
        return f"{self.value} {self.coords}"

    def length(self):
        #print(str(self))
        if self.parent is None:
            return 0
        else:
            return self.parent.length()+1

class Landscape:
    def __init__(self, topo):
        self.topo = topo
        self.juvenile = np.full(topo.shape, True)
    
    def run(self):
        start = np.where(self.topo == "S")
        coords = (start[0][0], start[1][0])
        searchqueue = [Node("a", coords, None)]
        self.juvenile[coords] = False
        #import pdb; pdb.set_trace()
        while True:
            #print([str(n) for n in searchqueue])
            node = searchqueue.pop(0)
            for direction in ((-1, 0), (0,1), (1,0), (0,-1)):
                c = sumcoords(node.coords, direction)
                if c[0] < 0 or c[1] <0: # numpy arrays may be indexed backwords
                    continue
                try:
                    h = self.topo[c]
                except(IndexError):
                    continue
                if h == "E":
                    if node.value >= "y":
                        return Node(h, c, node)
                elif self.juvenile[c] and ord(h) <= ord(node.value) +1:
                    searchqueue.append(Node(h, c, node))
                    self.juvenile[c] = False


def readfile(indata):
    lol = [[*row.rstrip('\n')] for row in indata]
    return np.array(lol)


with open(filename, "r") as indata:
    topo = readfile(indata) 
    target = Landscape(topo).run()
    print(target.length())
    
