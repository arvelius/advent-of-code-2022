#!/usr/bin/env python

import sys
from pdb import set_trace

if "-t" in sys.argv:
    test=True
    filename = "testdata.txt"
else:
    test=False
    filename = "input.txt"


def dropsand(occupied, ymax):
    count = 0
    while True:
        x = 500
        for y in range(ymax+1):
            if (x, y+1) in occupied:
                if (x-1, y+1) in occupied:
                    if (x+1, y+1) in occupied:
                        print(x,y)
                        occupied.add((x,y))
                        count += 1
                        break
                    else:
                        x += 1
                else:
                    x -= 1
        else:
            return count

def readfile(indata):
    occupied = set()
    ymax = 0
    for row in indata:
        corners = [pair.split(",") for pair in row.rstrip('\n').split(" -> ")]
        for i in range(1,len(corners)):
            (x1, x2, y1, y2) = (
                int(corners[i-1][0]),
                int(corners[i][0]),
                int(corners[i-1][1]),
                int(corners[i][1])
            )
            if x1 == x2:
                (yl, yh) = (min(y1,y2), max(y1,y2))
                if yh > ymax:
                    ymax = yh
                for j in range(yl,yh+1):
                    occupied.add((x1, j))
            else:
                if y1 > ymax:
                    ymax = y1
                (xl, xh) = (min(x1,x2), max(x1,x2))
                for j in range(xl,xh+1):
                    occupied.add((j, y1))

    if test:
        assert corners == [["503","4"],["502","4"],["502","9"],["494","9"]]
        assert (498,5) in occupied
        assert len(occupied) == 20
        assert (495,9) in occupied
        assert ymax == 9
    return occupied, ymax

with open(filename, "r") as indata:
    (occupied, ymax) = readfile(indata)
    print(dropsand(occupied, ymax))
