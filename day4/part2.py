def overlap(row):
    elf1, elf2 = row.split(",")
    (b1, e1) = elf1.split("-")
    (b2, e2) = elf2.split("-")
    b1 = int(b1)
    b2 = int(b2)
    e1 = int(e1)
    e2 = int(e2)

    if b1 >= b2 and b1 <= e2:
        return True
    elif b1 <= b2 and b2 <= e1:
        return True
    return False

with open("input.txt", 'r') as infile:
    retval = 0
    for line in infile:
        if overlap(line):
            retval += 1

print(str(retval))
