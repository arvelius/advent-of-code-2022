#!/usr/bin/env python

import sys

if "-t" in sys.argv:
    test=True
    filename = "testdata.txt"
else:
    test=False
    filename = "input.txt"

def readfile(indata):
    for row in indata:
        print(row.rstrip('\n'))

        
with open(filename, "r") as indata:
    readfile(indata)

        
