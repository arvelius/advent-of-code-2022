#!/usr/bin/env python

filename = "input.txt"

def uniq(instr):
    return len({c for c in instr}) == 4

assert uniq("abcd")
assert not uniq("abcc")

def start(instr):
    for i in range(len(instr)-4):
        if uniq(instr[i:i+4]):
            return i+4

assert start("mjqjpqmgbljsphdztnvjfqwrcgsmlb") == 7
assert start("bvwbjplbgvbhsrlpgdmjqwftvncz") == 5
assert start("nppdvjthqldpwncqszvftbrmjlhg") == 6


with open(filename, "r") as indata:
    print(start(indata.readline()))

        
