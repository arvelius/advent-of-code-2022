#!/usr/bin/env python

filename = "input.txt"

def uniq(instr):
    return len({c for c in instr}) == 14

def start(instr):
    for i in range(len(instr)-14):
        if uniq(instr[i:i+14]):
            return i+14

assert start("mjqjpqmgbljsphdztnvjfqwrcgsmlb") == 19
assert start("bvwbjplbgvbhsrlpgdmjqwftvncz") == 23
assert start("nppdvjthqldpwncqszvftbrmjlhg") == 23


with open(filename, "r") as indata:
    print(start(indata.readline()))

        
