#!/usr/bin/env python

from itertools import zip_longest
import sys

if "-t" in sys.argv:
    test=True
    filename = "testdata.txt"
else:
    test=False
    filename = "input.txt"

if "-2" in sys.argv:
    part = 2
else:
    part = 1
    
class Packet:
    def __init__(self, instr):
        self.parts = partsplitter(instr[1:-1])

    def __lt__(self, other):
        for (left, right) in zip_longest(self.parts, other.parts):
            if left is None:
                return True
            if right is None:
                return False
            if isinstance(left, Packet) and not isinstance(right, Packet):
                right = Packet(f"{[right]}")
            elif not isinstance(left, Packet) and isinstance(right, Packet):
                left = Packet(f"{[left]}")
            if left < right:
                return True
            if right < left:
                return False
        return False

    def __str__(self):
        return "[" + ",".join([str(p) for p in self.parts]) + "]"

    def __eq__(self, other):
        return str(self)==str(other)

def part1(indata):
    number = 0
    thesum = 0
    for row in indata:
        if row == '\n':
            continue
        number += 1
        left = Packet(row.rstrip('\n'))
        right = Packet(indata.readline().rstrip('\n'))
        if left < right:
            thesum += number
        print(number, left<right)
    return thesum

def part2(indata):
    outstr = ""
    decoder = ['[[2]]', '[[6]]']
    thelist = [Packet(x) for x in decoder]
    for row in indata:
        if row == '\n':
            continue
        thelist.append(Packet(row.rstrip('\n')))
    thelist.sort()
    #import pdb; pdb.set_trace()
    if test:
        for code in thelist:
            print(code)
    ans = 1
    for x in decoder:
        ans *= thelist.index(x)+1
    print(ans)
        

def partsplitter(instring):
    if instring == "":
        return list()
    nl = 0
    retlist = list()
    buf = ""
    for char in instring+",":
        if nl == 0 and char == ",":
            try:
                retlist.append(int(buf))
            except:
                retlist.append(Packet(buf))
            buf = ""
        else:
            buf += char
        if char == "[":
            nl += 1
        elif char == "]":
            nl -= 1
    return retlist

if test:
    assert partsplitter("") == []
    assert partsplitter("1,1,3") == [1,1,3]
    parts = partsplitter("[1],[2,3,4]")
    assert isinstance(parts[0], Packet)
    parts = partsplitter("1,[2,[3]]")
    assert parts[0] == 1
    assert parts[1].parts[1].parts[0] == 3
    parts = partsplitter("[[[3],2],1]")
    assert parts[0].parts[0].parts[0].parts[0] == 3
    assert parts[0].parts[1] == 1

with open(filename, "r") as indata:
    if part == 1:
        print(part1(indata))
    else:
        part2(indata)
        
