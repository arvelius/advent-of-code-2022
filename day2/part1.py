class RockPaperScissors:
    def __init__(self):
        self.score = 0

    def game(self, opponent, you):
        if you == "X":
            retval = 1
            if opponent == "A":
                retval += 3
            elif opponent == "C":
                retval += 6
        elif you == "Y":
            retval = 2
            if opponent == "B":
                retval += 3
            elif opponent == "A":
                retval += 6
        elif you == "Z":
            retval = 3
            if opponent == "C":
                retval += 3
            elif opponent == "B":
                retval += 6
        self.score += retval

rps = RockPaperScissors()

with open('input.txt') as guide:
    breakpoint()
    for line in guide:
        key = line.strip().split()
        rps.game(key[0], key[1])

print(rps.score)
