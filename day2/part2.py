class RockPaperScissors:
    def __init__(self):
        self.score = 0

    def game(self, opponent, outcome):
        if outcome == "X":
            retval = 0
            if opponent == "A":
                retval += 3
            elif opponent == "B":
                retval += 1
            elif opponent == "C":
                retval += 2
        elif outcome == "Y":
            retval = 3
            if opponent == "A":
                retval += 1
            elif opponent == "B":
                retval += 2
            elif opponent == "C":
                retval += 3
        elif outcome == "Z":
            retval = 6
            if opponent == "A":
                retval += 2
            elif opponent == "B":
                retval += 3
            elif opponent == "C":
                retval += 1
        self.score += retval

rps = RockPaperScissors()

with open('input.txt') as guide:
    breakpoint()
    for line in guide:
        key = line.strip().split()
        rps.game(key[0], key[1])

print(rps.score)
