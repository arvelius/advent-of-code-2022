#!/usr/bin/env python

import sys

#
if len(sys.argv)>1 and sys.argv[1] == "-t":
    test=True
    filename = "testdata.txt"
else:
    test=False
    filename = "input.txt"

def fliprows(indata):
    buf = list()
    for row in indata:
        if row == "\n":
            break
        buf.append(row)
    row = buf.pop()
    flip = list()
    for char in row:
        flip.append([char])
    for row in buf[::-1]:
        for i, char in enumerate(row):
            flip[i].append(char)
    return flip

def stackinit(indata):
    flip = fliprows(indata)
    stacks = dict()
    for row in flip:
        try:
            num = int(row[0])
        except:
            continue
        arr = list()
        for char in row[1:]:
            if char == ' ':
                break
            arr.append(char)
        stacks[num] = arr
    return stacks

def crane(stacks, indata):
    for row in indata:
        parts = row.split(' ')
        number = int(parts[1])
        origin = int(parts[3])
        target = int(parts[5])
        for i in range(number):
            stacks[target].append(stacks[origin].pop())
    return stacks

def result(stacks):
    resval=""
    for i, arr in stacks.items(): resval+=arr.pop()
    return resval

with open(filename, "r") as indata:
    stacks = stackinit(indata)
    stacks = crane(stacks, indata)

print(result(stacks))
        
