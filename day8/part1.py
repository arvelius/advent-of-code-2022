#!/usr/bin/env python

import numpy as np
import sys

if "-t" in sys.argv:
    test=True
    filename = "testdata.txt"
else:
    test=False
    filename = "input.txt"

def readfile(indata):
    lol = [[*row.rstrip('\n')] for row in indata]
    return np.array(lol, dtype=int)

def seenfromleft(forest):
    seen = np.zeros(forest.shape, dtype = np.bool)
    for i, row in enumerate(forest):
        high = -1
        for j, val in enumerate(row):
            seen[i][j] = val>high
            high = max(high, val)
    return seen

with open(filename, "r") as indata:
    forest = readfile(indata)
    seenleft = seenfromleft(forest)
    seenright = np.fliplr(seenfromleft(np.fliplr(forest)))
    seenabove = seenfromleft(forest.T).T
    seenbelow = np.fliplr(seenfromleft(np.fliplr(forest.T))).T
    seen = np.logical_or(
        np.logical_or(seenleft, seenright),
        np.logical_or(seenabove, seenbelow)
    )
    print(sum(sum(seen)))

    
