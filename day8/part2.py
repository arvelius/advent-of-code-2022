#!/usr/bin/env python

from functools import reduce
import numpy as np
from operator import mul
import sys

if "-t" in sys.argv:
    test=True
    filename = "testdata.txt"
else:
    test=False
    filename = "input.txt"


def readfile(indata):
    lol = [[*row.rstrip('\n')] for row in indata]
    return np.array(lol, dtype=int)

with open(filename, "r") as indata:
    forest = readfile(indata)

def score(forest, i, j):
    return reduce(mul, sights(forest, i, j))

def sights(forest, i, j):
    high = forest[i][j]
    for ii in range(i-1, -1 ,-1):
        if forest[ii][j]>=high:
            break
    up = i-ii
    for jj in range(j-1, -1 ,-1):
        if forest[i][jj]>=high:
            break
    left = j-jj
    for ii in range(i+1, forest.shape[0]):
        if forest[ii][j]>=high:
            break
    down = ii-i
    for jj in range(j+1, forest.shape[1]):
        if forest[i][jj]>=high:
            break
    right = jj-j
        
    return up, left, down, right

if test:
    assert sights(forest, 1, 2) == (1, 1, 2, 2)
    assert sights(forest, 3, 2) == (2, 2, 1, 2)

maxscore = 0
for i in range(1, forest.shape[0]-1):
    for j in range(1, forest.shape[1]-1):
        val = score(forest, i, j)
        if val>maxscore:
            maxscore = val

print(maxscore)
