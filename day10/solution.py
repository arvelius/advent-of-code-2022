#!/usr/bin/env python

import sys

if "-t" in sys.argv:
    test=True
    filename = "testdata.txt"
else:
    test=False
    filename = "input.txt"

def readfile(indata):
    register = [1]
    for row in indata:
        register.append(register[-1])
        try:
            register.append(register[-1]+int(row.split()[1]))
        except(IndexError):
            pass
    return register

with open(filename, "r") as indata:
    register = readfile(indata)

# part1
if test:
    assert register[20-1]*20 == 420
    assert register[60-1]*60 == 1140
    assert register[100-1]*100 == 1800
    assert register[140-1]*140 == 2940
    assert register[180-1]*180 == 2880
    assert register[220-1]*220 == 3960

thesum = 0
for c in [20, 60, 100, 140, 180, 220]:
    thesum += register[c-1]*c

print(thesum)

# part2
string = ""
for pos, reg in enumerate(register):
    pos = pos%40
    if pos in [reg-1, reg, reg+1]:
        mark = '#'
    else:
        mark = '.'
    string += mark
    if pos == 39:
        string += "\n"
print(string)
